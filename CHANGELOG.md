# changes by release
## 0.4.2
### added
* multiline output for alerts check

### fixes
* #8+

## 0.4.1
### fixes
* datasets_quota_usage, datasets_usage: are now using api query-filter which now fetches also all subdatasets instead of only the first level.
* zpool_usage: fixes negative percentage diff calculation

## 0.4.0
From this version onwords only api v2.0 will be used. Some checks may not work anymore with FreeNAS <11.3.

### features
* new optional parameter '--unit' which specifies the desired unit for the output - MB, GB,..

### other
* datasets_quota_usage: uses api v2.0 now instead of ssh and checks all datasets of a zpool where refquota is defined. renamed from 'dataset_quota_usage' to 'datasets_quota_usage'
* datasets_usage, zpool_usage: now uses api v2.0
* options: '-H' can be also used to define hostname


## 0.3.3
### fixes
* alerts: fixes service output when all alerts are dismissed

## 0.3.2
### fixes
* alerts, updates: now uses api v2.0

## 0.3.1
### fixes
* datasets_usage: increase the limit query param to 50

### other
* include and exclude option now uses regex instead of array

## 0.3.0
### features
* new modes:
  * zpool_iostat_read_iops
  * zpool_iostat_write_iops
  * zpool_iostat_read_bw
  * zpool_iostat_write_bw

### other
* updated tested versions

## 0.2.0
### features
* new modes:
 * zpool_usage
 * dataset_quota
 * arc_cache_miss_ratio
 * arc_cache_meta_usage
 * arc_cache_mru_ghost
 * arc_cache_mfu_ghost
 * io_requests_pending
 * disk_busy

### other
* no *https://* prefix necessary anymore for address
* renamed mode *datasets* to *datasets_usage*
* refactored some helper methods


## 0.1.1
### fixes
* *alert_check*: convert empty json output to OK status

### other
* catch all http responses instead of a few

## 0.1
* Initial release
